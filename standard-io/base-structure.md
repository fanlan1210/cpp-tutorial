# 基本架構
## 導入函式庫
在開始顯示文字之前，得先將其相關的功能函式給聲明在我們的檔案中。

而負責處理顯示和輸入的程式庫為`iostream`，可以使用`#include<函式庫名稱>`將其匯入。

範例:
```cpp
#include<iostream>
```

## 名稱空間(namespace)
> 註: 由於此部分牽涉到物件導向，所以在此只先簡單介紹其概念

在C++中，提供了**名稱空間**的概念，方便避開命名重複而產生衝突的問題。
名稱空間就好比是課本科目的名稱(ex:數學)，而每個課本裡可能都會有數個單元、小節。

而在接下來要使用的`cout`、`cin`等函式，被分類在`std`的名稱空間中，
假如我們要使用，就得完整地將其名稱空間與函式都寫出來，如以下範例:
```cpp
std::cout;
```

### using namespace
由於目前我們均會在`std`這個名稱空間中，為了避免每次使用都要再寫一次，
所以我們會直接撰寫一行程式碼，直接告訴程式我們接下來均會使用到它。
```cpp
using namespace std;
```

## 主程式
> 註: 由於此部分牽涉到函式的宣告與運用，所以在此只簡單介紹主函式的部分

當我們都準備好之後，接下來就是要宣告**主程式**。

主程式是主要執行程式的地方，一個C++程式一定要有`main`函式，也就是主程式，才能成功執行。
```cpp
int main()
{
    /* 程式碼 */
}
```

## 補充: 註解
當我們在編寫程式時，可以在程式檔裡面留下只用來讓開發者閱讀的訊息，
這種不會被電腦執行的訊息就稱為**註解**

註解又分為單行與多行註解，如以下範例:
```cpp
// 單行註解範例

/*
    多行
    註解
    範例
*/ 
```
