# 迴圈: while
## while迴圈的基本語法
```cpp
while (條件式){
    /* 程式碼 */
}
```
要建立while迴圈，基本上只需要1項元素:
* 條件式: 當條件式成立，while迴圈將會持續執行

## 基礎while迴圈範例程式
下面將改寫前一節用for迴圈將數字累加的程式，
從1開始，一直加到10，最後顯示加總的和。
使用while表示。
```cpp
#include<iostream>

using namespace std;

int main()
{
    int i,number;
    i=1;
    while (i<=10){
        number += i;
        i++;
    }
    cout<<number;
}
```
