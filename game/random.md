# 亂數生成
在遊戲中，隨機是一件極為重要的部分。

以下將介紹C++中亂數的基本生成與使用方式。

## 隨機數的使用: rand()
在C++中，最簡單的隨機數產生函式為`<cstdlib>`函式庫中的`rand()`

此函數將會透過運算產生0~*RAND_MAX*(常數)之間的整數。

## 亂數種子: srand()
由於隨機函數會需要一個初始數值來進行運算，<br>
所以我們透過預先使用亂數種子設定函式`srand()`，<br>
並帶入`<ctime>`函式庫的`time(NULL)`(取得現在時間)函式，<br>
使每次隨機演算的初始不同，確保每次產生的數字不會與之前重複。

以下為設定種子並輸出隨機數的範例:
```cpp
#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main(){
    srand( time(NULL) );
    cout<<rand();
}
```

## 取特定範圍的隨機數
利用除法取餘數運算子(`%`)，可以進一步取出特定範圍亂數。
```cpp
rand()%10     //範圍0~9
rand()%10 + 1 //範圍1~10
```