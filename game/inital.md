# 遊戲初始化
## 宣告所需變數
在開始戰鬥前，我們得先建立戰鬥時所需的各項基本數值:
* 勇者:血量、攻擊力
* 惡龍:血量、攻擊力
* 回合計數
```cpp
int hero_hp=1000,hero_atk=100; //勇者相關
int dragon_hp=3000,dragon_atk=50; //惡龍相關
int round; //回合數
```

## 回合計數
這裡使用for迴圈，作為每次回合的計算與重複執行。
```cpp
for(round=1;round>0;round++){
    cout<<"第"<<round<<"回合"<<endl;
    /* 對戰流程 */
}
```

## 遊戲介面顯示
接下來要顯示給玩家控制的介面，<br/>
基本上得顯示雙方各數值，<br/>
以及提供行動的選項與提示文字。

### 簡易型
簡易型的介面較易編寫，但整體顯示較不整齊。
```cpp
cout<<"勇者HP:"<<hero_hp<<endl;
cout<<"惡龍HP:"<<dragon_hp<<endl;
cout<<"選項:1.攻擊"<<endl;
cout<<"請輸入操作選項:";
```

### 一般型
透過導入`<iomanip>`這個函式庫，<br/>
可以讓我們對文字的顯示格式進行細部的調整。

這裡主要使用`setw()`來統一血量顯示的文字，<br/>
`setw()`是設定輸出的欄位長度函式，類似建立文字方塊的概念<br/>
括號內填入要設定的寬度，並接在輸出之變數前即可。

另外還有`left`和`right`用來進行文字的對齊，通常會和`setw()`一起使用，加在其前面。

```cpp
cout<<"勇者HP:"<<right<<setw(4)<<hero_hp<<endl;
cout<<"惡龍HP:"<<right<<setw(4)<<dragon_hp<<endl;
cout<<"選項:1.攻擊"<<endl;
```

## 接收玩家選擇
在前面的部份中，我們提示了玩家輸入操作選項，<br/>
接下來將實作接收部分。

這裡透過`cin`至一字串變數，儲存玩家選擇，並加入if做判斷。

另外為了避免玩家輸入不正確的數值，造成程式無法繼續運行，<br>
因此在此加入while無限迴圈。
```cpp
string selection;

while(true){
    cout<<"請輸入操作選項:";
    cin>>selection;
    if (selection == "1"){
        cout<<"勇者進行攻擊";

        break;
    }
    else{
        cout << "輸入錯誤!";
    }
}
```