# 戰鬥過程運算

## 傷害倍率

利用上一節提到的隨機函數，搭配判斷式，可以製作機率暴擊判斷程式:
```cpp
int atk_rate=1,random_num;
random_num = rand()%1;
if (random_num == 1)
{
    atk_rate = 2;
    cout<<"暴擊!"<<endl;
}
else
{
    atk_rate = 1;
}
```
上列程式使用隨機函數取範圍為0~1之間的整數，<br>
並儲存至`random_num`中，接下來透過判斷是否為特定數字(這裡為1)，<br>
決定是否改變傷害倍率並提示。

此範例的暴擊機率為50%。

## 傷害與血量更新

在決定好傷害倍率後，即可開始進行傷害計算。

```cpp
dragon_hp = dragon_hp - hero_atk * atk_rate;
cout<<"惡龍受到"<< hero_atk * atk_rate <<"點傷害!"<<endl;
```

而惡龍對玩家造成傷害之運算，基本上相同:
```cpp
hero_hp = hero_hp - dragon_atk;
cout<<"勇者受到"<< dragon_atk <<"點傷害!"<<endl;
```

## 回合結束統計
在回合結束前，我們得判斷勇者及惡龍的血量，<br>
如果其中一方血量耗盡，將提示玩家，並跳出無限迴圈，遊戲結束。
```cpp
if (hero_hp <= 0)
{
    cout<<"勇者被惡龍擊敗!"<<endl;
    cout<<"遊戲結束"<<endl;
    break;
}
else if (dragon_hp <= 0)
{
    cout<<"惡龍被勇者打倒!"<<endl;
    cout<<"遊戲結束"<<endl
    break;
}
```