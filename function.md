# 函式
數學中的函數，是將一數值帶入進行運算的過程。

而程式裡的函式，就類似於數學函數，<br>
可以將值帶入函式進行一系列運算，<br>
或只是單純呼叫使用函式的的程式碼，<br>

## 宣告函式
一般來說，函式的宣告可依擺放方式分為兩種，分別為**放在主函式前**與**放在主函式後**。

主函式(`main()`): 為程式執行時預設進入的函式

### 於主函式前
```cpp
型態 函式名稱(型態 變數1,型態 變數2 ...)
{
    /* 欲執行的程式碼 */
    return 值;
}
```
`return`為函式最後要回傳的值，回傳值必須與函式的型態相符，否則會出錯。

而小括號內可一併宣告欲傳入的變數，也可以留空表示不傳入任何值。

### 於主函式後
由於主函式將會呼叫其他函式，但此時尚未宣告其他函式，將會造成編譯錯誤。

於是得先在主函式之前，宣告其他函式的存在，稱為**原型宣告**。

```cpp
型態 變數名稱(型態 , 型態...); 
```
原型宣告完畢後，即可參考*於主函式前*的說明，實作函數之功能。

## 使用函式
```
函式名稱(變數1 , ...);
```

## 應用範例
下面為利用函式來進行兩整數的加總程式，使用將函數宣告在主函式後的方式。
```cpp
#include<iostream>

using namespace std;

int plus(int,int);

int main()
{
    int number;
    number = plus(3,5);
    cout << number;
}

int plus(int num1,int num2)
{
    return num1 + num2;
}

```
