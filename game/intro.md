# 基本資訊
接下來將實作一個回合制戰鬥遊戲，<br/>
類似於RPG遊戲中的戰鬥方式。

## 遊戲流程
勇者(玩家)和惡龍(電腦)輪流出招，<br/>
如果惡龍血量歸零，則勇者勝利。

## 備註
此遊戲改寫自[布丁的勇者鬥惡龍(Pudding's Dragon Quest)](https://github.com/fanlan1210/puddings_Dragon_Quest)，<br/>
點擊[這裡](http://163.23.148.69/~fanlan/pdq-online.php)可線上體驗。