# 迴圈: for
## for迴圈的基本語法
```cpp
for (初始變數;條件式;更新值){
    /* 程式碼 */
}
```
要建立for迴圈，基本上需要三項元素:
* 初始變數: 定義該for迴圈要使用的變數初始值
* 條件式: for迴圈執行的條件，該式成立時，for迴圈將會執行
* 更新值: 每次迴圈執行完時，變數須更新的值

## 基礎for迴圈範例程式
下面為一個將數字累加的程式，
從1開始，一直加到10，最後顯示加總的和。
```cpp
#include<iostream>

using namespace std;

int main()
{
    int i,number=0;
    for (i=1;i<=10;i++){
        number += i;
    }
    cout<<number;
}
```

## 進階練習
請嘗試寫出九九乘法表的程式