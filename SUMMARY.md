# Summary

## 語法

* [Introduction](README.md)
* [基本輸出入](standard-io/README.md)
  * [基本架構](standard-io/base-structure.md)
  * [輸出訊息: cout](standard-io/cout.md)
  * [獲取訊息: cin](standard-io/cin.md)
* [變數](variable.md)
* [條件判斷](judge/README.md)
  * [if](judge/if.md)
  * [switch](judge/switch.md)
* [迴圈](loop/README.md)
  * [for](loop/for.md)
  * [while](loop/while.md)
  * [補充](loop/misc.md)
* [陣列](array.md)
* [函式](function.md)

## 應用: 小遊戲實作

* [遊戲基本資訊](game/intro.md)
* [初始化](game/inital.md)
* [亂數生成](game/random.md)
* [戰鬥運算](game/battle.md)


