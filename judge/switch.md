# 條件判斷:switch
## switch的基本語法
在C++中，switch語句的語法:
```cpp
switch (變數){ 
    case 數字或字元:
        /* 程式碼 */
        break;
    default:
        /* 程式碼 */
```

`switch`會依序拿代入的變數來和每個`case`進行比對，<br/>
如果符合，將會執行冒號後的程式碼，直到`break`才跳出。<br/>
假如每個`case`均不符合，則執行`default`裡的程式敘述，類似if中的`else`。

### 延伸: break
`break`是switch-case語句中重要的**結束執行**部分，
如果在`case`後沒有加上`break`，將會繼續執行判斷。

範例:
```cpp
switch (number){
    case 1:
    case 2:
        cout<<number;
        break;
    default:
        cout<<"X";
}
```
在這個範例中，如果*number*為1或2，均會執行`case 2`中的敘述。

如果轉換成if語法，將會變成:
```cpp
if (number == 1 || number == 2){
    cout<<number;
}
else {
    cout<<"X";
}
```

## 基礎switch範例程式
下面為一個判斷分數等第的程式，<br/>
首先會提示使用者輸入0~100的分數，接者將其除以10，<br/>
利用switch依序判斷得分等級。
```cpp
#include<iostream>

using namespace std;

int main()
{
    int score;
    cout<<"請輸入得分(0~100): ";
    cin>>score;
    score = score / 10;
    switch (score){
        case 10:
            cout<<"等第為A";
            break;
        case 9:
            cout<<"等第為B";
            break;
        case 8:
            cout<<"等第為C";
            break;
        case 7:
            cout<<"等第為D";
            break;
        case 6:
            cout<<"等第為E";
            break;
        default:
            cout<<"不及格!";;
    }
}
```

